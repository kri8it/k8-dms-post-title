<?php
/*
	Plugin Name: K8 DMS Post Title
	Plugin URI: http://www.kri8it.com
	Description: Add a post title to the page
	Author: Farn van Zyl
	PageLines: true
	Version: 1.0.1
	Section: true
	Class Name: K8PostTitle
	Filter: component
	Loading: refresh
 
	K8 Bitbucket Plugin URI: https://bitbucket.org/kri8it/k8-dms-post-title
*/

/**
 * IMPORTANT
 * This tells wordpress to not load the class as DMS will do it later when the main sections API is available.
 * If you want to include PHP earlier like a normal plugin just add it above here.
 */

if( ! class_exists( 'PageLinesSectionFactory' ) )
	return;


class K8PostTitle extends PageLinesSection {
	
	function section_persistent(){
        		
    }
		
	function section_template() {
		
		//Get current post
		if( is_page() || is_single() ):
			
			global $post;
			
			$post_title = $this->opt( 'k8_post_title_override', array( 'default' => $post->post_title ) );
			$title_wrap = $this->opt( 'k8_post_title_wrap', array( 'default' => 'h2' )  );
			
			$post_title = sprintf( '<%s class="post-title">%s</%s>', $title_wrap, $post_title, $title_wrap );
			
			echo $post_title;
		
		else:
			
			echo '<p>This is not a single page or post.</p>';
			
		endif;
		
	}

	function section_opts(){
						
		$opts = array(
			array(
				'type'		=> 'multi',
				'key'		=> 'k8_post_title_settings',
				'col'		=> 1,
				'opts'		=> array(
					
					array(
						'key'		=> 'k8_post_title_post_link',						
						'type'		=> 'edit_post',
						'title'		=> __( 'Edit Post Content', 'pagelines' ),
						'label'		=>	__( '<i class="icon icon-edit"></i> Edit Post Info', 'pagelines' ),
						'help'		=> __( 'This section uses WordPress posts. Edit post information using WordPress admin.', 'pagelines' ),
						'classes'	=> 'btn-primary'
					),
					array(
						'key'			=> 'k8_post_title_override',
						'type' 			=> 'text',
						'label' 		=> __( 'Post Title Override', 'pagelines' ),
						'help' 			=> __( 'This will be be used instead of the current posts title. *Optional', 'pagelines' )
					),
					array(
						'type' 			=> 'select',
						'key'			=> 'k8_post_title_wrap',
						'label' 		=> __( 'Post Title Text Wrapper', 'pagelines' ),
						'default'		=> 'h2',
						'opts'			=> array(
							'h1'			=> array('name' => '&lt;h1&gt;'),
							'h2'			=> array('name' => '&lt;h2&gt;  (default)'),
							'h3'			=> array('name' => '&lt;h3&gt;'),
							'h4'			=> array('name' => '&lt;h4&gt;'),
							'h5'			=> array('name' => '&lt;h5&gt;'),
						)
					)					
				)
			)
		);
		return $opts;		
	}
}